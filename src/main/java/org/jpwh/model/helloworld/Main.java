package org.jpwh.model.helloworld;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

public class Main {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa");

		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		String name41 = "ms-glass004.1";
		javax.persistence.Query q1 = em.createQuery("SELECT c FROM DomainGf c WHERE name = :nameDomain");
		q1.setParameter("nameDomain", name41);
		DomainGf domain41 = null;
		try {
			domain41 = (DomainGf) q1.getSingleResult();
		} catch (NoResultException e) {
			System.out.println(e.getMessage());
			domain41 = new DomainGf();
			domain41.setName(name41);
		}

		System.out.println("----Found domain=" + domain41);
		System.out.println("try to clear");
		em.remove(domain41);
		tx.commit();
		tx.begin();
		domain41 = new DomainGf();
		domain41.setName(name41);
		domain41.setIpAddress("192.168.191.63");
		domain41.setEnsemble("80134");
		domain41.setLastUpdate(new Date());
		domain41.getUrls().add("ComponentMonthlyFeeHeartBeatURL = http://ms-glass004:5806/MonthlyFeeHeartBeatURL");
		domain41.getUrls().add("ComverseProxyWSRegionUrl = http://ms-rwebtst001:8022/Region.svc");
		domain41.getUrls().add("CRMOperationsUrl = http://ms-glass004:4455/CRMOperationsUrl");
		domain41.getUrls().add("DSIntegrationTroubleTicketURL = http://ms-glass004");
		domain41.getUrls().add("EDIProxyEDIUrl = https://vc-test.esphere.ru/service/wsdl/Beeline.wsdl");
		domain41.getUrls().add("GetCreditServiceSoapURL = http://ms-glass004:20000/GetCreditService");
		domain41.getNapiProxyUrls().add("ComponentMonthlyFeeNapiProxyURL = http://ms-glass004:8080/NAPIService/NAPIEjbBean");
		domain41.getNapiProxyUrls().add("DSIntegrationNAPIProxyURL = http://ms-vp0001-0003:8282/NAPIService/NAPIEjbBean");
		domain41.getNapiProxyUrls().add("ObsEmulatorNAPIProxyURL = http://ms-glass004:4043/NAPIService/NAPIEjbBean");
		domain41.getNapiProxyUrls().add("OnlinePaymentsNapiProxyURL = http://ms-pieeai002:8088/mockNAPIPortBinding");
		domain41.getNapiProxyUrls().add("SubscriberInfoNAPIProxyURL = http://ms-glass004:4043/NAPIService/NAPIEjbBean");
		domain41.getNapiProxyUrls().add("SubscriberSMNapiProxyUrl = http://ms-vp0001-0003:8282/NAPIService/NAPIEjbBean");
		
		Component component = new Component();
		component.setName("AttributeMapperCA");
		component.setVersion("2.0.0.16");
		component.setState("Started");
		component.setDomainGf(domain41);
		em.persist(component);		
		
		
		tx.commit();
		em.close();
		System.exit(0);
	}

}
