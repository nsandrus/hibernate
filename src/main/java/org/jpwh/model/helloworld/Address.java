package org.jpwh.model.helloworld;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Address {
	
	@Id
	@GeneratedValue
	private long id;
	
	private String zipCodes;
	
	@ManyToOne
//	@JoinColumn(name="message")
	private Message message;
	
	public String getZipcode() {
		return zipCodes;
	}
	public void setZipcode(String zipcode) {
		this.zipCodes = zipcode;
	}
	

	public Message getMessage() {
		return message;
	}
	public void setMessage(Message message) {
		this.message = message;
	}
	

}
