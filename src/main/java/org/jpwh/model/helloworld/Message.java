package org.jpwh.model.helloworld;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "MESSAGES")
public class Message {
	@Id
	@GeneratedValue
	private long id;
	
	private String text;
	
	@OneToMany(mappedBy="message", cascade = CascadeType.ALL,orphanRemoval = true)
	private Set <Address> zipCodes = new HashSet<>();

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Set<Address> getZipcode() {
		return zipCodes;
	}

	public void setZipcode(Set<Address> zipcode) {
		this.zipCodes = zipcode;
	}

	public Long getId() {
		return id;
	}

	
	
	
}