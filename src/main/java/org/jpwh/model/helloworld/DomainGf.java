package org.jpwh.model.helloworld;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
@Table(name = "DOMAINS")
public class DomainGf {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long Id;
	
	
	private String name;
	private String ipAddress;
	private String ensemble;
	private Date lastUpdate; 
	
	@ElementCollection
	private Set<String> urls = new HashSet<>();
	
	@ElementCollection
	private Set<String> napiProxyUrls = new HashSet<>();

	@OneToMany(mappedBy = "domainGf",cascade =CascadeType.ALL)
	private Set<Component> components = new HashSet<>();

	
	
	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getEnsemble() {
		return ensemble;
	}

	public void setEnsemble(String ensemble) {
		this.ensemble = ensemble;
	}

	public Set<String> getUrls() {
		return urls;
	}

	public void setUrls(Set<String> urls) {
		this.urls = urls;
	}

	public Set<String> getNapiProxyUrls() {
		return napiProxyUrls;
	}

	public void setNapiProxyUrls(Set<String> napiProxyUrls) {
		this.napiProxyUrls = napiProxyUrls;
	}

	public Set<Component> getComponents() {
		return components;
	}

	public void setComponents(Set<Component> components) {
		this.components = components;
	}

	@Override
	public String toString() {
		return "DomainGf [Id=" + Id + ", name=" + name + ", ipAddress=" + ipAddress + ", ensemble=" + ensemble + ", lastUpdate=" + lastUpdate + ", urls=" + urls
				+ ", napiProxyUrls=" + napiProxyUrls + ", components=" + components + "]";
	}

	
	
	
	
}
