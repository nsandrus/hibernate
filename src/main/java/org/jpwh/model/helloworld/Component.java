package org.jpwh.model.helloworld;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "COMPONENTS")
public class Component {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long Id;
	
	private String name;
	private String version;
	private String state;
	
	@ManyToOne(cascade =CascadeType.ALL)
	private DomainGf domainGf;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
	public DomainGf getDomainGf() {
		return domainGf;
	}
	public void setDomainGf(DomainGf domainGf) {
		this.domainGf = domainGf;
	}
	@Override
	public String toString() {
		return "Component [Id=" + Id + ", name=" + name + ", version=" + version + ", state=" + state + "]\n";
	}
	
	

}
